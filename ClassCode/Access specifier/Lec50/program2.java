//Access Specifier -	1 default	2 private	3 public	4 protacted
class Core2Web{
	int  Nocourse=8;
	private String favcourse="Java";
	public String ClassName="Core2Web";

	void disp(){
		System.out.println(Nocourse);
		System.out.println(favcourse);
	}
}

class User{
	public static void main(String[] args){
		Core2Web obj = new Core2Web();
		System.out.println(obj.Nocourse);		//default variable access in folder only
		System.out.println(obj.favcourse);		//private variable only access in class
		System.out.println(obj.ClassName);		//public varialble access in other folder also
	}
}
