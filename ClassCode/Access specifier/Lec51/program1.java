class Demo{
	int x=10;
	private int y=20;

	void fun(){
		System.out.println(x);
		System.out.println(y);
	}
}
class MainDemo{
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.fun();

		System.out.println(obj.x);
		System.out.println(obj.y);		//Error y variable is private
		System.out.println(x);			//Error x is not in Scope (can not find Symbol)
		System.out.println(y);			//Error y is not in Scope(can not find Symbol)
	}
}
