class Core2web{
	int x=10;
	static int y=20;
	void fun(){
		int z=30;
	}
}
/* 			After compile
 *
 * class Core2web{
 * 	Core2web(){				//constructor
 * 		super();   //invokedSpecial
 * 		int x=10;  //bipush 10
 * 	}
 * 	void fun(){
 * 		int y=20;  //bipush 20
 * 	}
 * 	static{
 * 		int z=30;   //bipush 30
 * 	}
 * }
 */
