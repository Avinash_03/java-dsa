/*	Exception handling 
 *	Two type of exception handling
 *	1)compile time		2)Runtime 
 */
//IOException (Compile time)
import java.io.*;
class Demo{
	public static void main(String[] args){  // public static void main(String[] args)throws IOException{- to remove exception

		System.out.println("Start Main");

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		String st = br.readLine();				// error: unreported exception IOException; must be caught or declared to be thrown
		System.out.println(st);
		System.out.println("End Main");
	}
}
