class Demo{
	public static void main(String[] args){
		System.out.println("Start Main");
		try{
			System.out.println(10/0);		//try need to write when user enter value
								//in try block code called as risky code
		}catch(ArithmeticException obj){
			System.out.println("Exception occured");	//in catch block code called as Handling code
		}
		System.out.println("End Main");
	}
}
/*	try catch use for normal termination
 */
