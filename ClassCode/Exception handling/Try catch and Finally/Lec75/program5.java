//FINALLY
class Demo{
	void m1(){
	}
	void m2(){
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.m1();
		obj = null;
		try{
			obj.m2();
		}catch(NullPointerException obj1){
			System.out.println("Here");
		}finally{					//Exception hapend or not finally block execute anyway
			System.out.println("Connection Closed");
		}
		System.out.println("End Main");
	}
}
