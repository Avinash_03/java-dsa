import java.util.Scanner;
class DataOverflowException extends RuntimeException{
	DataOverflowException(String msg){
		super(msg);
	}
}
class DataUnderflowException extends RuntimeException{
	DataUnderflowException(String msg){
		super(msg);
	}
}
class Array{
	public static void main(String[] args){
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Integer Value");
		System.out.println("Note : 0 < Element <100");
		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]<0)
				throw new DataUnderflowException("please Enter No Greater than 0");
			if(arr[i]>100)
				throw new DataOverflowException("please Enter No Under 100");
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+" ");
		}
	}
}
