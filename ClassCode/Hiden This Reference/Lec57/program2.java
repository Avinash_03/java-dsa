//This Refrence
class Demo{
	int x=10;
	Demo(){
		System.out.println("No-Args Constructor");
	}
	Demo(int x){
		System.out.println("In Para Constructor");
	}
	public static void main(String[] args){
		Demo obj =new Demo();
		Demo obj1 =new Demo(10);
	}
}
// O/P - No-Args Constructor
// 	 In Para Constructor
