class Demo{
	int x=10;
	Demo(){
		System.out.println("In No-Args Constructor");	
	}
	Demo(int x){
		this();				//this call alwas of first line when we can use this() then 
						//invokespecial call not present (we can use only one this or super)	
		System.out.println("In Para Constructor");	
	}
	public static void main(String[] args){
		Demo obj2 = new Demo(50);
	}
}
