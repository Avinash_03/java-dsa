class Demo{
	int x=10;
	Demo(){
		this(10);
		System.out.println("In No-Args Constructor");	
	}
	Demo(int x){
		this();				//this recursive call not allow
		System.out.println("In Para Constructor");	
	}
	public static void main(String[] args){
		Demo obj2 = new Demo(50);
	}
}
