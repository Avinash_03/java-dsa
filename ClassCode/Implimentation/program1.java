//Method 1 : concat
import java.io.*;
class Concat{
	static void Concat(String str1,String str2){
		char ch[]=str1.toCharArray();
		char ch1[]=str2.toCharArray();
		int size=ch.length+ch1.length;
		char ch2[]=new char[size];

		for(int i=0;i<ch.length;i++){
			ch2[i]=ch[i];
		}
		for(int i=0;i<ch1.length;i++){
			ch2[i+ch.length]=ch1[i];
		}
		for(int i=0;i<ch2.length;i++){
			System.out.print(ch2[i]);
		}
		System.out.println();
	}
	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter String:");
		String str2 = sbr.readLine();
		Concat(str1,str2);
	}
}
