//Method 10 : replace(char olderchar new char);
import java.io.*;
class replaceDemo{
	static String myreplace(String str1,char old,char newch){
		char ch[]=str1.toCharArray();
		for(int i=0;i<ch.length;i++){
			if(ch[i]==old)
				ch[i]=newch;
		}

		String ret = new String(ch);

		return ret;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();

		System.out.println("Enter Old Character:");
		char ch = (char)sbr.read();

		sbr.skip(1);
		System.out.println("Enter Character for repalce:");
		char ch1 = (char)sbr.read();
		sbr.skip(1);

		String str2 = myreplace(str1,ch,ch1);
		System.out.println("changed String : "+str2);
	}
}
