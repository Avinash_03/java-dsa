//Method 11 : SubString(int index);
import java.io.*;
class SubStringDemo{
	static String mysubString(String str1,int index){
		char ch[]=str1.toCharArray();
		char ch1[]=new char[index];
		
		for(int i=0;i<index;i++){
			ch1[i]=ch[i];
		}
		String str =new String(ch1);
		return str;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter Number for SubString:");
		int index=Integer.parseInt(sbr.readLine());
		String str2=mysubString(str1,index);
		System.out.println("SubString is: "+str2);
	}
}
