//Method 2 : length
import java.io.*;
class LengthDemo{
	static int strLength(String str1){
		char ch[]=str1.toCharArray();
		int cnt=0;
		for(int i=0;i<ch.length;i++){
			cnt++;
		}
		return cnt;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		int len=strLength(str1);
		System.out.println("Enter String Length is: "+len);
	}
}
