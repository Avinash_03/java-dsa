//Method 3 : charAt(int index);
import java.io.*;
class CharAtDemo{
	static char myCharAt(String str1,int index){
		char ch[]=str1.toCharArray();
		return ch[index];
	}
	public static void main(String[] args)throws IOException{
		int index;
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter Index:");
		index=Integer.parseInt(sbr.readLine());
		char ch=myCharAt(str1,index);
		System.out.println("character at index "+index+ " is "+ch);
	}
}
