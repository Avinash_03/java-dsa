//Method 4 : compareTo(string str);
import java.io.*;
class CompareTo{
	static int myCompareTo(String str1,String str2){
		char ch[]=str1.toCharArray();
		char ch1[]=str2.toCharArray();
		for(int i=0;i<ch.length;i++){
			if(ch[i]!=ch1[i])
				return ch[i]-ch1[i];
		}
		return 0;
	}
	static int strLength(String str1){
                char ch[]=str1.toCharArray();
                int cnt=0;
                for(int i=0;i<ch.length;i++){
                        cnt++;
                }
                return cnt;
        }

	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter String:");
		String str2 = sbr.readLine();
		int len1=strLength(str1);
		int len2=strLength(str2);
		if(len1==len2){
			int dis=myCompareTo(str1,str2);
			System.out.println(dis);
		}else{
			System.out.println("String length not same");
		}
	}
}
