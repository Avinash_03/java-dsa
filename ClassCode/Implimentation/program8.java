//Method 8 : indexof(int ch,int fromindex);
import java.io.*;
class IndexofDemo{
	static int myIndexOf(String str1,char ch,int index){
		char ch1[] = str1.toCharArray();
		for(int i=index;i<ch1.length;i++){
			if(ch1[i]==ch)
				return i;
		}
		return -1;
	}
	static int strLength(String str1){
                char ch[]=str1.toCharArray();
                int cnt=0;
                for(int i=0;i<ch.length;i++){
                        cnt++;
                }
                return cnt;
        }

	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter character to search:");
		char ch = (char)(sbr.read());
		sbr.skip(1);
		System.out.println("Enter index to start search:");
		int index=Integer.parseInt(sbr.readLine());
		int dis=myIndexOf(str1,ch,index);
		System.out.println("character found at index "+dis);
	}
}
