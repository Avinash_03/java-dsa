//Method 9 : LastIndexof(int ch,int Toindex);
import java.io.*;
class LastindexofDemo{
	static int myLastIndexOf(String str1,char ch,int index){
		char ch1[] = str1.toCharArray();
		int store=-1;
		for(int i=0;i<=index;i++){
			if(ch1[i]==ch)
				store=i;
		}
		return store;
	}
	static int strLength(String str1){
                char ch[]=str1.toCharArray();
                int cnt=0;
                for(int i=0;i<ch.length;i++){
                        cnt++;
                }
                return cnt;
        }

	public static void main(String[] args)throws IOException{
		BufferedReader sbr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String:");
		String str1 = sbr.readLine();
		System.out.println("Enter character to search:");
		char ch = (char)(sbr.read());
		sbr.skip(1);
		System.out.println("Enter index to start search:");
		int index=Integer.parseInt(sbr.readLine());
		int dis=myLastIndexOf(str1,ch,index);
		System.out.println("character found at index "+dis);
	}
}
