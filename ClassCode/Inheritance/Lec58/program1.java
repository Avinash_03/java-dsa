/*	For class only two formatSpacifire Allow 1) Default 2) public
 *	For Constructor All formatSpacifire Allo but Static not allo to constructor
 */
class ICC{
	ICC(){
		System.out.println("In ICC Constructor");
	}
}
class BCCI extends ICC{
	BCCI(){
		System.out.println("In BCCI Constructor");
	}
}
class Client{
	public static void main(String[] args){
		BCCI obj = new BCCI();
	}
}
/* O/P -	In ICC Constructor
		In BCCI Constructor
 */
