class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void ParentProperty(){
		System.out.println("Flat,Car,Gold");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Child obj1=new Child();
		Parent obj2=new Child();	//we can creat object using parent class 
	//	Child obj3=new Parent();	//error: incompatible types: Parent cannot be converted to Child. we can not creat object using child class
		obj1.ParentProperty();
	}
}
