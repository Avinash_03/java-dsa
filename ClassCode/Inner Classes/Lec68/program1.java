/*	Four Type Of Inner Class
 *	1)Normal	2)Method Local	     3)static or static nested		4)Annonymous
 *								  		-1)Normal
 *								 		-2)Parameterized
 */
//Normal Inner class
//Syntax :-
/*
class Outer{
	class Inner{
	}
}
*/
class Outer{
	class Inner{
		void m1(){
			System.out.println("In -m1- Inner");
		}
	}
	void m2(){
		System.out.println("In -m2- Outer");
	}
}
class Client{
	public static void main(String[] args){
		Outer obj = new Outer();
		Outer.Inner obj1 = obj.new Inner();
		obj.m2();
		obj1.m1();
		//Outer.Inner obj = new Outer().new Inner();
	}
}

