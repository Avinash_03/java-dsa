class Outer{

	void m1(){
		class Inner{
			void m1(){
				System.out.println("In Inner m1");
			}
		}
		class Demo{
			void m2(){
				System.out.println("In Demo m2");
			}
		}
	}
}
class Client{
	public static void main(String[] args){
		Outer obj = new Outer();
		obj.m1();
	}
}
