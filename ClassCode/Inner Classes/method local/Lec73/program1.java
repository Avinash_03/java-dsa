class Outer{
	void M1(){
		System.out.println("In -M1- Outer");
			class Inner{
				void M1(){
					System.out.println("In -M1- Inner");
				}
			}
	}
	void M2(){
		System.out.println("In -M2- Outer");
	}
}
class Client{
	public static void main(String[] args){
		Outer obj = new Outer();
		obj.M1();
		obj.M2();
	}
}
