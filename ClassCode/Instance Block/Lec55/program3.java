//INSTANCE BLOCK
class Demo{
	int x=0;
	Demo(){
		System.out.println("Constructor");
	}
	{	//Instance Block ,its a part of constructor and it takes place the top of constructor
		System.out.println("In Instance Block 1");
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		System.out.println("Main");
	}
	{
		System.out.println("In Instance Block 2");
	}
}
