//singleton
class Singleton{
	static Singleton obj = new Singleton();
	private Singleton(){
		System.out.println("Constructor");
	}
	static Singleton getobject(){
		return obj;
	}
}
class Client{
	public static void main(String[] args){
		Singleton obj1 = Singleton.getobject();	
		System.out.println(obj1);
		Singleton obj2 = Singleton.getobject();
		System.out.println(obj2);
		Singleton obj3 = Singleton.getobject();
		System.out.println(obj3);
	}
}
