interface Demo{
	void fun();
	void gun();
}
class DemoChild implements Demo{
	public void fun(){
		System.out.println("In Fun");
	}
	public void gun(){
		System.out.println("In Gun");
	}
}
class client{
	public static void main(String[] args){
		Demo obj = new DemoChild();
		obj.fun();
		obj.gun();
	}
}
/*	1) interface have 100% abstraction
 *	2) Method by default public abstract
 *	3) Constructor not present
 *	4) Not able to create object
 *	5) We can extends the interface
 *	6) We can give body to interface method using dafault and static 
 *	7) static interface can access using name of interface
 */
