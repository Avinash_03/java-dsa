//Multiple Interface in Java
interface Demo{
	void fun();
}
interface Demo2{
	void fun();
}
class DemoChild implements Demo,Demo2{
	public void fun(){
		System.out.println("In Fun Child");
	}
}
class Client{
	public static void main(String[] args){
		DemoChild obj = new DemoChild();
		obj.fun();
	}
}
