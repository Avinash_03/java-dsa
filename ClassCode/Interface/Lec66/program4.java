interface Demo{
	void fun();
	default void gun(){
	}
	static void run(){
	}
}
interface Demo1 {
	static void m1(){
		System.out.println("Demo1 - m1");
	}
}
interface Demo3 extends Demo,Demo2{
}
class DemoChild implements Demo3{			//error: DemoChild is not abstract and does not override abstract method fun() in Dem
	public static void main(String[] args){
		Demo1 obj = new DemoChild();		//error: incompatible types: DemoChild cannot be converted to Demo1
		obj.m1();				// error: illegal static interface method call
	}
}
