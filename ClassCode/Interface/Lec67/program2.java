interface Demo{
	default void fun(){
	System.out.println("In- Fun Demo");
	}
}
interface Demo2{
	default void fun(){
		System.out.println("In -Fun Demo2");
	}
}
class DemoChild implements Demo,Demo2{
	public void fun(){
		System.out.println("In Fun DemoChild");
	}
}
class Client{
	public static void main(String[] args){
		DemoChild obj = new DemoChild();
		obj.fun();
		Demo2 obj2 = new DemoChild();
		obj2.fun();
	}
}
