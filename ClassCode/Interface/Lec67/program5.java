interface Demo{
	static void fun(){
		System.out.println("In Fun Demo");
	}
}
interface Demo2{
	static void fun(){
		System.out.println("In Fun Demo2");
	}
}
class DemoChild implements Demo,Demo2{
	void fun(){
		System.out.println("In Fun DemoChild");
		Demo.fun();
		Demo2.fun();
	}
}
class Client {
	public static void main(String[] args){
		DemoChild obj = new DemoChild();
		obj.fun();
	}
}
