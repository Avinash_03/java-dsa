interface A{
	int x = 10;
}
interface B{
	int x=20;
}
class Child implements A,B{
	int x=30;
	void fun(){
		System.out.println(x);
	}
}
class Client{
	public static void main(String[] args){
		Child obj = new Child();
		obj.fun();
	}
}
/*	Marker Interface
 *	1) serializable
 *	2) random access
 *	3) clonable
 *	4) EventListner
 *	5) Marker interface madhe methods nastat
 *	6) this interfece use only for functionality
 */
