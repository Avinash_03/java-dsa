class Demo{

	public static void main(String[] args){
		float f1 = 7.5;                   //correct - 7.5f
		float f2 = 7.5;

		System.out.println(f1);         //incompatible type
		System.out.println(f2);         //possible lossy conversion double to flopat
	}
}
