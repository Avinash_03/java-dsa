/*  Print Max no from two input 
 *  Assume: inputs are not equal
 */
import java.util.*;
class Max{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);
		System.out.println("Enter X value:");
		int x = obj.nextInt();
		System.out.println("Enter Y value:");
		int y = obj.nextInt();

		if(x>y)
			System.out.println(x+" is greater");
		else
			System.out.println(y+" is greater");
	}
}
