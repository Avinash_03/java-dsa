/* if else ladder
 * Human Temprature
 */
import java.util.*;
class Temp{
	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter Body Temprature");
		float unit = obj.nextFloat();
		if(unit > 98.6f){
			System.out.println("High");
		}else if(unit < 98.0f){
			System.out.println("Low");
		}else{
			System.out.println("Normal");

		}
	}
}	
