/* Find no Divisible by 4 or not
 */
import java.util.*;
class Div{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);
		System.out.println("Enter Number:");

		int Num = obj.nextInt();

		if(Num % 4 == 0)
			System.out.println("Divisible");
		else
			System.out.println("Not Divisible");
	}
}
