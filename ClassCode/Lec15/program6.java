// find number even or odd
import java.util.*;
class Find{
	
	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter Number");

		int Num = obj.nextInt();

		if(Num % 2 == 0)
			System.out.println(Num+" is even");
		else
			System.out.println(Num+" is odd");
	}
}
