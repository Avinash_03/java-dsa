/* num divisible by 3 then print fizz
 * num divisible by 5 then print buzz
 * num divisible by both then print fiz-buzz
 * num not divisible by both then print Not divisible by both
 */
import java.util.*;
class Demo{

	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter Number");

		int Num = obj.nextInt();

		if(Num % 3 == 0 && Num % 5 == 0)
			System.out.println("fizz-buzz");
		else if(Num % 3 ==0)
			System.out.println("fizz");
		else if(Num % 5 ==0)
			System.out.println("buzz");
		else
			System.out.println("Not divisible by both");

	}
}
