/* Electricity Units
 * units <= 100 then rate is 1 per unit
 * units > 100 then rate is 2 per unit
 */
import java.util.*;
class Electricity{
	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter Units:");

		int Unit = obj.nextInt();

		if(Unit <= 10)
			System.out.println(Unit);
		else
			System.out.println(100+(Unit-100)*2);
	}
}
