//sum of all digit of number 
class SumDigit{

	public static void main(String[] args){
		int Var=6531;
		int sum=0;

		while(Var != 0){
			int rem = Var % 10;
			Var/=10;
			sum+=rem;
		}
		System.out.println(sum);
	}
}
