//print Reverse of number
class Digit{

	public static void main(String[] args){
		int num = 6531;
		int rev = 0;
		while(num != 0){
			int rem = num % 10;
			rev = rev * 10 + rem;
			num/=10;
		}
		System.out.println(rev);
	}
}
