//scope
class Core2web{

	public static void main(String[] args){
		int x= 10;
		{
			int y= 20;
			System.out.println(x+" "+y);  //10 20
		}
		{
			System.out.println(x+""+y);  //Error
		}
		System.out.println(x+""+y);          //Error
	}
}
