//switch case
class SwitchDemo{
	public static void main(String[] args){
		int x= 5;
		switch(x){
			case 1: 
				System.out.println("one");
				break;
			case 2: 
				System.out.println("Two");
				break;
			case 5: 
				System.out.println("first -5");
				break;
			case 5: 
				System.out.println("second -5");	//error
				break;
			case 2: 
				System.out.println("second -2");	//error
				break;
			default:
				System.out.println("No match");
				break;

		}
		System.out.println("After Switch");
	}
}
