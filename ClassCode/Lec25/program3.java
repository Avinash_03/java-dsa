//switch case
class SwitchDemo{
	public static void main(String[] args){
		int x= 65;
		switch(x){
			case 'A': 
				System.out.println("char -A");
				break;
			case 65: 
				System.out.println("Num-65");
				break;
			case 'B': 
				System.out.println("char -B");
				break;
			case 66: 
				System.out.println("Num -66");
				break;
			default:
				System.out.println("No match");
				break;

		}
		System.out.println("After Switch");
	}
}
