//switch case
class SwitchDemo{
	public static void main(String[] args){
		int x= 3;
		int a= 1;
		int b= 2;
		switch(x){
			case a: 
				System.out.println("one");
			case b: 
				System.out.println("Two");
			case a+b: 
				System.out.println("Three");
			case b+b: 
				System.out.println("Four");
			case b+b+a: 				//error used variable not a constat value
				System.out.println("Five");
			default:
				System.out.println("No match");

		}
		System.out.println("After Switch");
	}
}
