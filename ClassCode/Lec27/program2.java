//input output in java
import java.util.Scanner;
class ScannerDemo{

	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);

		System.out.println("Enter Dream Company Name");
		String str = obj.next();
		System.out.println("Enter Dream package");
		float pack = obj.nextFloat();

		System.out.println("Dream Company Name - "+str);
		System.out.println("Dream Package - "+pack);
	}
}
