import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Socity Name");
		String str = br.readLine();

		System.out.println("Enter Wing");
		char wing = (char)br.read();

		br.skip(1);
		System.out.println("Enter Flat No");
		int flat = Integer.parseInt(br.readLine());

		System.out.println("Socity Name - "+str);
		System.out.println("Wing - "+wing);
		System.out.println("Flat No - "+flat);

	}
}
