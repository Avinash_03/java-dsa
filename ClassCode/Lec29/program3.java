import java.io.*;
import java.util.*;

class Demo{
	
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Society Name,Wing,Flat No:");
		String str=br.readLine();
		System.out.println(str);

		StringTokenizer obj = new StringTokenizer(str," ");

		String str1 = obj.nextToken();
		String str2 = obj.nextToken();
		String str3 = obj.nextToken();

		System.out.println("Society = "+str1);
		System.out.println("Wing ="+str2);
		System.out.println("Flat No ="+str3);

	}
}
