import java.io.*;
import java.util.*;

class Demo{
	
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Batsman Name,Grade,Run rate,Income:");
		String str=br.readLine();
		System.out.println(str);

		StringTokenizer obj = new StringTokenizer(str," ");

		String str1 = obj.nextToken();
		char str2 = obj.nextToken().charAt(0);
		int RR = Integer.parseInt(obj.nextToken());
		float inc = Float.parseFloat(obj.nextToken());

		System.out.println("Batsman Name = "+str1);
		System.out.println("Grade ="+str2);
		System.out.println("Run rate ="+RR);
		System.out.println("Income ="+inc);

	}
}
