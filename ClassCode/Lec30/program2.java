//static ,Non static Methods
class MethodDemo{
	public static void main(String[] args){
		fun();
		gun();
	}
	static void fun(){
		System.out.println("In fun Method");
	}
	void gun(){						//non static Methode gun() cannot be referenced from a static
								//context
		System.out.println("In gun Method");
	}
}
