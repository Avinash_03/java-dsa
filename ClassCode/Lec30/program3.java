//static ,Non static Methods
class MethodDemo{
	public static void main(String[] args){
		fun();
		MethodDemo g = new MethodDemo();
		g.gun();
	}
	static void fun(){
		System.out.println("In fun Method");
	}
	void gun(){				

		System.out.println("In gun Method");
	}
}
