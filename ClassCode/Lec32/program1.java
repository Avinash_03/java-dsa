class Demo{

	void fun(int x){
		System.out.println(x);
	}
	public static void main(String[] args){
		System.out.println("In main");
		Demo d = new Demo();
		d.fun();	//actual and formal argument lists differ in length
		System.out.println("End main");
	}
}
