class Demo{

	void fun(float x){
		System.out.println("In fun");
		System.out.println(x);
	}
	public static void main(String[] args){
		
		Demo d = new Demo();
		d.fun(10);
		d.fun('A');
		d.fun(10.5f);
	}
}
