class Demo{

	void fun(int x){
		int y= x+10;
	}
	public static void main(String[] args){
		
		Demo d = new Demo();
		int a = d.fun(10);	 //incompatible types: void cannot be converted to int
		System.out.println(a);
	}
}
