import java.io.*;
class EveArr{
	public static void main(String[] args)throws IOException{

		int cnt=0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array Size");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter array element");

		for(int i=0; i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				cnt++;
		}
		System.out.println("Even Element Count = "+cnt);
	}
}
