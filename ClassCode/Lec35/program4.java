
class Demo{
	public static void main(String[] args){
		int arr1[]=new int[]{10,20,30,40,200};
		int arr2[]={10,20,30,40,200};

		Demo arr3[]=new Demo[4];

		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr2[0]));

		System.out.println(System.identityHashCode(arr1[4]));
		System.out.println(System.identityHashCode(arr2[4]));

		System.out.println(System.identityHashCode(arr1));
		System.out.println(System.identityHashCode(arr2));

		System.out.println(arr1);
		System.out.println(arr2);
	}
}
