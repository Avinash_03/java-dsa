class JaggedArray{

	public static void main(String[] args){
		int arr[][]={{1,2,3},{4,5},{7}};		//jagged array initialization method 1
		int arr1[][]=new int[3][];			//jagged array initialization method 1

		arr[0]=new int[]{1,2,3};
		arr[1]=new int[]{4,5};
		arr[2]=new int[]{7};
	}
}
