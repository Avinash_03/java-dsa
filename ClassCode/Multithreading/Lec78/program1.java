class Mythread extends Thread{
	public void run(){
		System.out.println("In Run");
		System.out.println("Thread = "+Thread.currentThread().getName());
	}
	public void start(){							//Tyanchi start method override kelya vr aaplya methodla call jato aani main thread to code exetute karto naki navin create zalela thread
		System.out.println("In Mythread start");
		run();
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Mythread obj = new Mythread();
		obj.start();
		System.out.println("MainThread = "+Thread.currentThread().getName());
	}
}
