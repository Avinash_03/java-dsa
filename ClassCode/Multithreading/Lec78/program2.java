class Demo extends Thread{
	public void run(){
		System.out.println("Thread = "+Thread.currentThread().getName());
	}
}
class Mythread extends Thread{
	public void run(){
		System.out.println("Thread = "+Thread.currentThread().getName());
		Demo obj = new Demo();
		obj.start();
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Mythread obj = new Mythread();
		obj.start();
	}
}
