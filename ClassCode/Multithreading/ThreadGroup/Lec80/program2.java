class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup gt = new ThreadGroup("C2W");
		Mythread obj1=new Mythread(gt,"C");
		Mythread obj2=new Mythread(gt,"JAVA");
		Mythread obj3=new Mythread(gt,"PYTHON");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
