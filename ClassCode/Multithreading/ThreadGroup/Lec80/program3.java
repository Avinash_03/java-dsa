class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup gt = new ThreadGroup("C2W");
		Mythread obj1=new Mythread(gt,"C");
		Mythread obj2=new Mythread(gt,"JAVA");
		Mythread obj3=new Mythread(gt,"PYTHON");

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup gt1 = new ThreadGroup(gt,"Incubater");
		Mythread obj4=new Mythread(gt1,"Flutter");
		Mythread obj5=new Mythread(gt1,"ReactJS");
		Mythread obj6=new Mythread(gt1,"SpringBoot");

		obj4.start();
		obj5.start();
		obj6.start();
	}
}
