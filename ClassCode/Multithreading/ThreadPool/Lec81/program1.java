class Mythread extends Thread{
	Mythread (ThreadGroup tg,String str ){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}	
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup tg = new ThreadGroup("India");
		Mythread Mt1 = new Mythread(tg,"Maha");
		Mythread Mt2 = new Mythread(tg,"Goa");
		
		Mt1.start();
		Mt2.start();

		ThreadGroup tg1 = new ThreadGroup(tg,"Pakistan");
		Mythread Mt3 = new Mythread(tg1,"Karachi");
		Mythread Mt4 = new Mythread(tg1,"Lahor");
		
		Mt3.start();
		Mt4.start();
		
		ThreadGroup tg2 = new ThreadGroup(tg,"Bangladesh");
		Mythread Mt5 = new Mythread(tg2,"Dhaka");
		Mythread Mt6 = new Mythread(tg2,"Rangpur");
		
		Mt5.start();
		Mt6.start();



	}
}
