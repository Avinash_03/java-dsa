class Mythread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(8000);
		}catch(InterruptedException ie){
		}
	}
}	
class ThreadGroupDemo{
	public static void main(String[] args){
		
		ThreadGroup tg = new ThreadGroup("India");
		Mythread Mt1 = new Mythread();
		Mythread Mt2 = new Mythread();

		Thread t1 = new Thread(tg,Mt1,"Maha");
		Thread t2 = new Thread(tg,Mt2,"Goa");
		
		t1.start();
		t2.start();

		ThreadGroup tg1 = new ThreadGroup(tg,"Pakistan");
		Mythread Mt3 = new Mythread();
		Mythread Mt4 = new Mythread();
		
		Thread t3 = new Thread(tg1,Mt3,"Karachi");
		Thread t4 = new Thread(tg1,Mt4,"Lahor");
		
		t3.start();
		t4.start();
		
		ThreadGroup tg2 = new ThreadGroup(tg,"Bangladesh");
		Mythread Mt5 = new Mythread();
		Mythread Mt6 = new Mythread();
		
		Thread t5 = new Thread(tg2,Mt5,"Dhaka");
		Thread t6 = new Thread(tg2,Mt6,"Rajeshahi");
		
		t5.start();
		t6.start();

		tg1.interrupt();
		System.out.println(tg.activeCount());
		System.out.println(tg.activeGroupCount());

	}
}
