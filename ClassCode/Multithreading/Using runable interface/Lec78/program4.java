//priority of thread
class Mythread extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
		Mythread obj1 = new Mythread();
		obj1.start();

		t.setPriority(7);

		Mythread obj2 = new Mythread();
		obj2.start();
	}
}
