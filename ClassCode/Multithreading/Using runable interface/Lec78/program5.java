//priority of thread
class Mythread extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
		Mythread obj1 = new Mythread();
		obj1.start();

		obj1.start();          //Exception in thread "main" java.lang.IllegalThreadStateException
	}
}
