/*POLYMORPHISM -TWO TYPES -> 1)OVERLODING(IN SAME CLASS)	2)OVERRIDING(IN INHERITANCE)
 */
//1)OVERLODING-
class Demo{
	void fun(int x){
		System.out.println(x);
	}
	void fun(int y){
		System.out.println(y);
	}
}
/*	Error-method fun(int)(method signature) is already defined in class Demo
 */
