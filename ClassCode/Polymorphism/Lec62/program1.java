//override
class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void Property(){
		System.out.println("Home,Car,Gold");
	}
	void marry(){
		System.out.println("Depika");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
	void marry(){
		System.out.println("Alia");		//override parent method
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.Property();
		obj.marry();
	}
}
