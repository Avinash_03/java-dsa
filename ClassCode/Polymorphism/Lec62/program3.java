class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
	void gun(){
		System.out.println("Child gun");
	}
}
class Client{
	public static void main(String[] args){
		Parent obj = new Child();
		Child obj1 = new Parent();		//error incopatible type parent cannot be converted to child
	
	}
}
