class Demo{
	void fun(int x, float y){
		System.out.println("Int -Float paramiter");
	}
	void fun(float x,int y){
		System.out.println("Float - Int paramiter");
	}
}
class client{
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.fun(10,10);			//Error - reference to fun is ambiguous
	}
}
