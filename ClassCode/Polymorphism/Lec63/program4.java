//synario A
class Demo{
	void fun(String str){
		System.out.println("String");
	}
	void fun(StringBuffer str){
		System.out.println("StringBuffered");
	}
}
class clinet{
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.fun("C2W");
		obj.fun(new StringBuffer("C2W"));
		obj.fun(null);			// error: reference to fun is ambiguous
	}
}
