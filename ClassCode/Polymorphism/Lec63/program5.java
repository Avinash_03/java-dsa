//synario B
class Demo{
	void fun(Object obj){
		System.out.println("Object");
	}
	void fun(String str){
		System.out.println("String");
	}
}
class client{
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.fun("C2W");
		obj.fun(new StringBuffer("C2W"));
		obj.fun(null);			//No Error
	}
}
