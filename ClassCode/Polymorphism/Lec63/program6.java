
class Parent{
	char fun(){
		System.out.println("Parent Fun");
		return 'A';
	}
}
class Child extends Parent{
	int fun(){					//Error: return type int is not compatible with char
		System.out.println("Child Fun");
		return '0';
	}
}
class Client{
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
