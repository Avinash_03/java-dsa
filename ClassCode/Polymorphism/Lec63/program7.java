//Covarint return type (Override madhe chalt)
class Parent{
	Object fun(){
		Object obj = new Object();
		System.out.println("Parent Fun");
		return obj;
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("Child Fun");
		return "Avi";
	}
}
class Client{
	public static void main(String[] args){
		Parent p= new Child();
		p.fun();
	}
}
