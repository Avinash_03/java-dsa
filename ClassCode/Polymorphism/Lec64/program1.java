//ACCESS SPECIFIER IN OVERRIDING
class Parent{
	public void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	void fun(){					//Error:attempting to assign weaker access privileges; was public 
		System.out.println("Child Fun");
	}
}
class Client {
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
