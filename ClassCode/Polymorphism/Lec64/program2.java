class Parent{
	private void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("Child Fun");
	}
}
class Client {
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();			//error: fun() has private access in Parent
	}
}
