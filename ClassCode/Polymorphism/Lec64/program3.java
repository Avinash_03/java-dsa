//final modifier in overriding
class Parent{
	final void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	void fun(){								//Error: overridden method is final
		System.out.println("Child Fun");
	}
}
class Client{
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
