class Demo{

	static{
		System.out.println("Static Block");
	}
	public static void main(String[] args){
		System.out.println("Main method");
	}
}
/*		O/P-	Static Block
 *			Main method
 */
