class Demo{
	int x=10;
	static int y=20;
	static{
		System.out.println("Static Block 1");
	}
	public static void main(String[] args){
		System.out.println("main method");
		Demo obj = new Demo();
		System.out.println(obj.x);
	}
	static {
		System.out.println("Static Block 2");
		System.out.println(y);
	}
}
/*	O/P-	Static Block 1
 *		Static Block 2
 *		20
 *		main method
 *		10
 */
