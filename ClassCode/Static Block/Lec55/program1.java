class Demo{
	static {
		System.out.println("Static Block 1");
	}
	public static void main(String[] args){
		System.out.println("In main");
	}
}
class Client{
	static {
		System.out.println("Static Block 2");
	}
	public static void main(String[] args){
		System.out.println("In Client Main");
	}
	static {
		System.out.println("Static Block 3");
	}
}
/*	O/P -	java Client.class
 *		Static Block 2
 *		Static Block 3
 *		In Client Main
 *
 *	O/P -	java Demo.class
 *		Static Block 1
 *		In main
 */
