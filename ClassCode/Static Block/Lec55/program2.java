// java suport global static variable
class Demo{
	static int x = 10;
	static {
		static int y =20;	//error illegal start expression
	}
	void fun(){
		static int z =20;	//error illegal start expression
	}
	static void gun(){
		static int x =20;	//error illegal start expression
	}
}
