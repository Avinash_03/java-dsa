class Parent{
	int x=10;
	static int y=20;
	static {
		System.out.println("In Parent Static Block");
	}
	Parent(){
		System.out.println("In Constructor");
	}
	void methodone(){

		System.out.println(x);
		System.out.println(x);
	}
	static void methodtwo(){
		System.out.println(y);
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
	}
	Child(){
		System.out.println("In Child Construction");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.methodone();
		obj.methodtwo();
	}
}
