//when parent class and in child class same variable and methods are present then we can access parent data using super.
class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("Parent");
	}
}
class Child extends Parent{
	int x=10;
	static int y=60;
	Child(){
		System.out.println("Child");
	}
	void access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}

}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.access();
	}
}
