/*Mothod 3 : charAt(int index)
 * description :-it returns the character located at specified index with in the given string
 * parameter :- integer
 * return type :-char
*/
class Demo{
	public static void main(String[] args){
		String str1="Avi";
		String str2="Ekhande";

		System.out.println(str1.charAt(0));
	}

}
