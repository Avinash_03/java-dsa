/*Mothod 4:	compareTo(String str2);
 * description :- it compares the str1 and str2 (case sensetive) if both the strings are equal it returns 0 otherwise returns the comparision
 * parameter :-string(second String)
 * return type :-integer
*/
class Demo{
	public static void main(String[] args){
		String str1="Avi";
		String str2="avi";

		System.out.println(str1.compareTo(str2));
	}

}
