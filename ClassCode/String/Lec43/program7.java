/*Mothod 5: compareToIgnorecase()
 * description :- it compare Str1 and Str2 (case Insensetive)
 * parameter :-string
 * return type :-integer
*/
class Demo{
	public static void main(String[] args){
		String str1="Avi";
		String str2="avi";

		System.out.println(str1.compareToIgnoreCase(str2));
	}

}
