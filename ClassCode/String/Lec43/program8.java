/*Mothod 6: public boolean equals(object anobject)
 * description :-predicate which compares and object to this ,this is true only for strings returns true if an object is sementicaly equal to this
 * parameter :-object (anObject)
 * return type :-boolean
*/
class Demo{
	public static void main(String[] args){
		String str1="Avi";
		String str2=new String("Avi");

		System.out.println(str1.equals(str2));
	}

}
