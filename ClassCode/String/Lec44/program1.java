import java.io.*;
class StrLength{

	int myStrLen(String str1){
		char ch[]=str1.toCharArray();
		int cnt=0;
		for(int x:ch){
			cnt++;
		}
		return cnt;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter First String: ");
		String str1 = br.readLine();
		System.out.print("Enter Second String: ");
		String str2 = br.readLine();

		StrLength sl=new StrLength();

		if(sl.myStrLen(str1)==sl.myStrLen(str2)){
			System.out.println("String equal: ");
		}else{
			System.out.println("String Not equal: ");
		}
	}
}
