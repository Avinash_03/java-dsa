/*	Method :- public boolean equalsIgnoreCase(String anotherString);
 *	Des:-compares a String To this String ignoring case
 *	Parameter:-string
 *	Retuen type:- boolean
 */
class equalsIgnoreCaseDemo{
	public static void main(String[] args){
		String str1="Avi";
		String str2="avi";

		System.out.println(str1.equalsIgnoreCase(str2));
	}
}
