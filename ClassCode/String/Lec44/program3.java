/*	Method:- public int indexOf(char ch,int fromIndex);
 *	Description:-Find first instance of the character in the given string
 *	Parameter:-character(char to find),integer (index to start search)
 *	Return Type:-integer
 */
class indexOfDemo{
	public static void main(String[] args){
		String str1="Avinash";
		System.out.println(str1.indexOf('s',0));		//5	
		System.out.println(str1.indexOf('s',3));		//5
	}
}
