/*	Method:- public substring (int index);
 *	Description:-create substaring of the given string starting at aspecified index and ending at the end of given string
 *	Parameter:-integer(index of the string)
 *	Return Type:-string
 */
class substringDemo{
	public static void main(String[] args){
		String str1="Avi Ekhande";
		System.out.println(str1.substring(0,3));	//take before second index
		System.out.println(str1.substring(4));		//take from index
	}
}
