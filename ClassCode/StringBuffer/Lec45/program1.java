class Demo{

	public static void main(String[] args){

		StringBuffer sb = new StringBuffer();
		System.out.println(sb.capacity());	//16
		System.out.println(sb);			//Null String

		sb.append("Shashi");
		System.out.println(sb.capacity());	//16 total capacity
		System.out.println(sb);			//Shashi
	}
}
