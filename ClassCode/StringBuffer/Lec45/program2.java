class Demo{

	public static void main(String[] args){

		StringBuffer sb = new StringBuffer();
		System.out.println(sb.capacity());	//16
		System.out.println(sb);			//Null String

		sb.append("Shashi");
		System.out.println(sb.capacity());	//16 total capacity
		System.out.println(sb);			//Shashi
							//
		sb.append("Bagal");
		System.out.println(sb.capacity());	//16 total capacity
		System.out.println(sb);			//ShashiBagal
							//
		sb.append("core2web");
		System.out.println(sb.capacity());	//34 total capacity
		System.out.println(sb);			//ShashiBagalcore2web
	}
}
