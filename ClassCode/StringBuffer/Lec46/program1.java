class Demo{
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer(100);

		sb.append("Biencaps");
		sb.append("Inqubator");
		System.out.println(sb);			//BiencapsInqubator
		System.out.println(sb.capacity());	//100 total capacity
		sb.append("core2web");
		System.out.println(sb);			//BiencapsInqubatorcore2web
		System.out.println(sb.capacity());	//100 total capacity
	}
}
