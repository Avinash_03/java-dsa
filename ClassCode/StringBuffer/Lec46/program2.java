class SBDemo{

	public static void main(String[] args){
		String str1="shashi";
		String str2=new String("Bagal");
		StringBuffer str3 = new StringBuffer("core2Web");

		//String str4=str1.append(str3);	can not find symbol
		//String str4=str3.append(str1);	StringBuffere can not be converted into string
		StringBuffer str4=str3.append(str1);	//change store at str3 

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
	}
}
