abstract class frinchies{
	void recipi(){
		System.out.println("Same");
	}
	abstract void price();
}
class fri1 extends frinchies{
	void price(){
		System.out.println("Diff price");
	}
}
class Client{
	public static void main(String[] args){
		fri1 obj = new fri1();
		obj.price();
		obj.recipi();
	}
}
/*	1) abstract have 0 - 100% abstraction
 *	2) need to give abstract keyword to abstract method
 *	3) Constructor present
 *	4) not able to create object 
 */
