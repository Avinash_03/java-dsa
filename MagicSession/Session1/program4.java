//cnt of odd digits in Number
class Cnt{

	public static void main(String[] args){

		int x= 942111423;
		int cnt=0;
		while(x != 0){
			int rem = x%10;
			x/=10;
			if(rem % 2 != 0)
		 		cnt++;
		}
		System.out.println(cnt);
	}
}
