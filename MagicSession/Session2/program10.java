// print prime no in given range
import java.io.*;
class PrimeN{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Start No ");
		int S= Integer.parseInt(br.readLine());
		System.out.println("Enter End No ");
		int E= Integer.parseInt(br.readLine());

		for(int i=S;i<=E;i++){
			int cnt =0;
			for(int j=1;j<=i;j++){
				if(i%j==0)
					cnt++;
			}
		if(cnt==2)
			System.out.print(i+" ");
		}
		System.out.println();
		
	}
}
