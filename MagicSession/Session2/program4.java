//print all even no in reverse order and odd in standard way
import java.io.*;

class EVe{

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Start No");
		int s= Integer.parseInt(br.readLine());
		System.out.println("Enter End No");
		int e= Integer.parseInt(br.readLine());
		for(int i = e; i>=s; i--){
			if(i%2==0)
				System.out.print(i+" ");
		}
		System.out.println();
		for(int i = s; i<=e; i++){
			if(i%2==1)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
