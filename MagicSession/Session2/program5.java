/*		0
 *		1	1
 *		2	3	5	
 *		8	13	21	34
 */
import java.io.*;
class Pattern{

	public static void main(String[] args)throws IOException{
		int x= 0;
		int y= 1;
		int ans = 1;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter row");
		int row = Integer.parseInt(br.readLine());
		for(int i= 1; i<=row; i++){
			for(int j= 1; j<=i; j++){
				System.out.print(x+" ");
				x=y;
				y=ans;
				ans=x+y;
			}
		System.out.println();
		}
	}
}
