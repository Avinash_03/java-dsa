//print character distance
import java.io.*;
class CharD{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter char");
		char ch = (char)(br.read());
		br.skip(1);
                System.out.println("Enter char");
		char ch1 = (char)(br.read());
		int x;

		if(ch>ch1){
			x=ch-ch1;
		}else{
			x= ch1-ch;
		}
		System.out.println("The difference between "+ch+" and "+ch1 +" is "+x);
	}
}
