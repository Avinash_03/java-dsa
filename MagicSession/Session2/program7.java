/*		O
 *		14	13
 *		L	K	J
 *		9	8	7	6
 *		E	D	C	B	A
 */
import java.io.*;
class Pattern{

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row");
		int row = Integer.parseInt(br.readLine());
		int Num=(row*row+row)/2;
		char ch = (char)(Num+64);

		for(int i = 1; i<=row;i++){
			for(int j = 1; j<=i; j++){
				if(row%2==1){
					if(i%2==1){
						System.out.print(ch+"\t");
					}else{
						System.out.print(Num+"\t");
					}
				}else{
					if(i%2==0){
						System.out.print(ch+"\t");
					}else{
						System.out.print(Num+"\t");
					}
				}
				ch--;
				Num--;
			}
		System.out.println();
		}
	}
}
