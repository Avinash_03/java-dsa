/*	$
 *	@	@
 *	&	&	&
 *	#	#	#	#
 */
import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Row");
		int row = Integer.parseInt(br.readLine());

		int s=1;
		for(int i= 1; i<=row;i++){
			for(int j= 1; j<=i;j++){
				if(s==1){
					System.out.print("$ ");
				}else if(s==2){
					System.out.print("@ ");
				}else if(s==3){
					System.out.print("& ");
				}else{
					System.out.print("# ");
					s=0;
				}
			}
			s++;
		System.out.println();
		}
	}
}
