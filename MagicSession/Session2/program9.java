//take no and add the factorial of each digit
import java.io.*;
class Addfact{

	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Number:");
		int N=Integer.parseInt(br.readLine());
		int N1= N;
		int sum=0;
		while(N!=0){
			int fact=1;
			int rem =N%10;
			for(int i=1;i<=rem;i++){
				fact*=i;
			}
			sum+=fact;
			N=N/10;
		}
		System.out.println("Addition of Factorial of each digit from "+ N1 +" = "+sum);
	}
}
