//print product of odd index in array
import java.io.*;
class ProArr{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		int mult=1;

		System.out.println("Enter Element:");
		for(int i=0; i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(i%2==1)
				mult*=arr[i];
		}
		System.out.println("Odd Index Sum = :" +mult);
	}
}
