//print element how divisible by 5
import java.io.*;
class DivArr{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter Element:");
		for(int i=0; i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Divisible by 5 Elements :");
		for(int i=0; i<arr.length;i++){
			if(arr[i]%5==0)
				System.out.println(arr[i]+" ");
		}
	}
}
