import java.io.*;
class EveOddArr{
	public static void main(String[] args)throws IOException{

		int Esum=0;
		int Osum=0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array Size");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter array element");

		for(int i=0; i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				Esum+=arr[i];
			else
				Osum+=arr[i];
		}
		System.out.println("Even Element Sum = "+Esum);
		System.out.println("Odd Element Sum = "+Osum);
	}
}
