//search element and print that no index
import java.io.*;
class SerArr{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array Size:");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];
               
                System.out.println("Enter Element:");
                for(int i=0; i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter Element to search :");
		int ser=Integer.parseInt(br.readLine());

		for(int i=0; i<arr.length;i++){
			if(ser==arr[i])
				System.out.println("Element found at index: "+i);
		}
        }
}

