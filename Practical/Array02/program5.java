//minimum element in array
import java.io.*;
class MinArr{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array Size:");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];
                int min=0;

                System.out.println("Enter Element:");
                for(int i=0; i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		min= arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]<min)
				min=arr[i];
		}
                System.out.println("min Element = :" +min);
        }
}

