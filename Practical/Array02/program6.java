//maximum element in array
import java.io.*;
class MaxArr{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array Size:");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];
                int max=0;

                System.out.println("Enter Element:");
                for(int i=0; i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		max= arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max)
				max=arr[i];
		}
                System.out.println("max Element = :" +max);
        }
}

