import java.io.*;

class Demo{
	static void Cnt(int arr[],int size){
	
		for(int i=0; i<arr.length;i++){
			int store=arr[i];
			int cnt=0;
			while(store!=0){
				cnt++;
				store=store/10;
			}
			System.out.println(cnt+" ");
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		Cnt(arr,size);
	}
}
