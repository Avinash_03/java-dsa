import java.io.*;
class Min{

	static void minNo(int arr[]){
		int min=arr[0];
		for(int i = 0; i<arr.length;i++){
			for(int j=1;j<=arr[i]/2;j++){
				if(min>arr[i])
					min=arr[i];
			}
		}
		System.out.println(min+" ");
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size =Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		minNo(arr);
	}
}
