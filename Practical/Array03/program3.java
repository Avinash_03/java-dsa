import java.io.*;

class Demo{
	static void Comp(int arr[]){
	
		for(int i=0; i<arr.length;i++){
			int cnt=0;
			for(int j=1; j<arr[i];j++){
				if(arr[i]%j==0)
					cnt++;
				if(cnt==2){
					System.out.println("composite "+arr[i]+ " found at index "+i); 
					break;
				}
			}
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		Comp(arr);
	}
}
