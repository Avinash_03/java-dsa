import java.io.*;

class Demo{
	static void Prime(int arr[]){
	
		for(int i=0; i<arr.length;i++){
			int cnt=0;
			for(int j=1; j<arr[i];j++){
				if(arr[i]%j==0)
					cnt++;
			}
			if(cnt==1){
				System.out.println("prime no "+arr[i]+ " found at index "+i); 
			}
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		Prime(arr);
	}
}
