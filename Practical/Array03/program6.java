import java.io.*;
class isPelin{
	static void isPelin(int arr[]){
		for(int i = 0; i<arr.length;i++){
			int store=arr[i];
			int rev=0;
			while(store!=0){
				int rem = store%10;
				rev=rev*10+rem;
				store=store/10;
			}
			if(rev==arr[i])
				System.out.println("pelindrom No "+ rev +" found at index "+i);
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size =Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		isPelin(arr);
	}
}
