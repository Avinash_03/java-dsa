import java.io.*;
class Strong{

	static void strongNo(int arr[]){
		for(int i = 0; i<arr.length;i++){
			int store=arr[i];
			int sum=0;
			while(store!=0){
				int fact=1;
				for(int j=1;j<=store%10;j++){
					fact*=j;
				}
				sum+=fact;
				store/=10;
			}
			if(sum==arr[i])
				System.out.println(" Strong No "+sum+" found at index "+i);
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size =Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		strongNo(arr);
	}
}
