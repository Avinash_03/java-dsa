import java.io.*;
class armst{
	static void armStrongNo(int arr[]){
		for(int i = 0; i<arr.length;i++){
			int store=arr[i];
			int sum=0;
			int cnt=0;
			while(store!=0){
				cnt++;
				store/=10;
			}
			int st=arr[i];
			while(st!=0){
				int rem=st%10;
				int mult=1;
				for(int j=1;j<=cnt;j++){
					mult=mult*rem;
				}
				sum+=mult;
				st/=10;
			}
			if(sum==arr[i])
				System.out.println(" armStrong No "+sum+" found at index "+i);
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size =Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		armStrongNo(arr);
	}
}
