import java.io.*;
class Max{

	static void maxNo(int arr[]){
			int max=arr[0];
		for(int i = 0; i<arr.length;i++){
			for(int j=1;j<=arr[i]/2;j++){
				if(max<arr[i])
					max=arr[i];
			}
		}
		System.out.println(max+" ");
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size =Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		maxNo(arr);
	}
}
