//print month and days in month
class Month{

	public static void main(String[] args){
		int month = 12;
		if(month <= 0 && month > 12){
			System.out.println("Invalid Month");
		}else if(month==1){
			System.out.println("jan Month has 31 days");
		}else if(month==2){
			System.out.println("feb Month has 29 days");
		}else if(month==3){
			System.out.println("mar Month has 31 days");
		}else if(month==4){
			System.out.println("apr Month has 30 days");
		}else if(month==5){
			System.out.println("may Month has 31 days");
		}else if(month==6){
			System.out.println("jun Month has 30 days");
		}else if(month==7){
			System.out.println("jul Month has 31 days");
		}else if(month==8){
			System.out.println("aug Month has 30 days");
		}else if(month==9){
			System.out.println("sep Month has 31 days");
		}else if(month==10){
			System.out.println("oct Month has 30 days");
		}else if(month==11){
			System.out.println("nov Month has 31 days");
		}else{
			System.out.println("dec Month has 30 days");
		}
	}
}
