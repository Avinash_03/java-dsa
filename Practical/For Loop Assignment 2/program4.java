//cnt of odd digits in Number
class Cnt{

	public static void main(String[] args){

		int x;
		int cnt=0;
		for(x=942111423; x != 0; x/=10){
			int rem = x%10;
			x/=10;
			if(rem % 2 != 0)
		 		cnt++;
		}
		System.out.println(cnt);
	}
}
