//print Reverse of number
class Digit{

	public static void main(String[] args){
		int num;
		int rev = 0;
		for(num=942111423; num != 0; num/=10){
			int rem = num % 10;
			rev = rev * 10 + rem;
		}
		System.out.println(rev);
	}
}
