/*	10
 *	9	8
 *	7	6	5
 *	4	3	2	1
 *
 */
class Pattern{
	public static void main(String[] args){
		int row=4;
		int Num=(4*4+4)/2;
		for(int i = 1; i<= row; i++){
			for(int j =1; j<=i; j++){
				System.out.print(Num+" ");
				Num--;
			}
			System.out.println();
		}
	}
}
