/*	10
 *	I	H
 *	7	6	5
 *	D	C	B	A
 *
 */
class Pattern{
	public static void main(String[] args){
		int row = 4;
		int x= (row*row+row)/2;
		char ch =(char)((row+row*row)/2+64);
		for(int i = 1; i<= row; i++){
			for(int j =1; j<=i; j++){
				if(i%2==0){
					System.out.print(ch+" ");
					ch--;
				}else{
					System.out.print(x+" ");
					x--;
				}
			}
			System.out.println();
		}
	}
}
