/*	1
 *	8	9
 *	27	16	125
 *	64	25	216	49
 *
 */
class Pattern{
	public static void main(String[] args){
		int row = 4;
		for(int i = 1; i<= row; i++){
			int Num = i;
			for(int j =1; j<=i; j++){
				if(j%2==1){
					System.out.print(Num*Num*Num +" ");
				}else{
					System.out.print(Num*Num +" ");
				}
				Num++;
			}
			System.out.println();
		}
	}
}
