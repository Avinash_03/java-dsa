/*	1 2 3 4 
 *	2 3 4 5 
 *	3 4 5 6 
 *	4 5 6 7
 */

class Demo{

	public static void main(String[] args){

		int num=1;
		for(int i=1; i<=4; i++){
			int num1=num;
			for(int j=1; j<=4; j++){
				System.out.print(num1+" ");
				num1++;
			}
			num++;
			System.out.println();
		}
	}
}
