/*	1	2	9
 *	4	25	6
 *	49	8	81
 */

class Demo{

	public static void main(String[] args){

		int x= 1;
		for(int i=1; i<=3; i++){
			for(int j=1; j<=3; j++){
				if(i==j || i+j == 4){
					System.out.print(x*x+"\t");
				}else{
					System.out.print(x+"\t");
				}
				x++;
			}
			System.out.println();
		}
	}
}
